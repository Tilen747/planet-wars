var express = require('express');
var app = express();

var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');
//var util = require('util'); //not needed only for memory usage

var Planet = require(__dirname + "/server/Planet.js");
var Ship = require(__dirname + "/server/Ship.js");
var helpFun = require(__dirname + "/server/helperFun.js");

/**
 * Object of rooms.
 * @type {{}}
 */
var rooms = {}; //{ id name: 'TheBigOne', users : [], planets: [], ships: [] }

/**
 * List of rooms ID's (for faster looping).
 * @type {Array}
 */
var roomsCatch = []

/**
 * If debug mode is enabled.
 * @type {boolean}
 */
var debug = (process.argv.slice(2)[0] == "debug" || process.argv.slice(2)[1] == "debug") ? true : false;

//Implement new log option.
console.debug = function (msg) {
    if (debug) console.log(msg)
}

/**
 * Find free room or create new one.
 * @returns {Number}
 * @constructor
 */
function FindRoom() {
    var roomId = undefined;

    for (var key in rooms) {
        if (rooms[key].players.length < rooms[key].maxUsers && rooms[key].password === null) {
            roomId = key
        }
    }

    if (roomId === undefined)
        roomId = CreateRoom("Public room " + Date.now(), 8, null);

    return roomId
}

/**
 * Create new room.
 * @param name
 * @param maxUsers
 * @param password
 * @returns {number} - room id
 * @constructor
 */
function CreateRoom(name, maxUsers, password) {
    console.debug("creating new room")
    var nextRoomId = 1;
    while (true) {
        if (rooms[nextRoomId] === undefined)
            break;
        nextRoomId++;
    }

    roomsCatch.push(nextRoomId)
    rooms[nextRoomId] = new Room(name, maxUsers, password, nextRoomId)
    return nextRoomId
}

/**
 * Room object.
 * @param name
 * @param maxUsers
 * @param password
 * @param roomId
 * @constructor
 */
function Room(name, maxUsers, password, roomId) {


    this.id = roomId
    this.name = name
    this.maxUsers = maxUsers
    this.password = password
    this.players = []
    this.planets = []
    this.ships = []
    this.nextPlanet = 1 //counter
    this.nextShip = 1 //counter
    this.nextPlayerId = 1  //counter
    this.colorGen_start = 0  //counter
    this.deleteMe = false //when set to true room will be deleted

    this.frozen = false //do not update room until not false (used for private rooms)

    this.blackHole = {
        x: 0,
        y: 0
    }

    /**
     * List of planets in line to be destroyed after their time has come. (ordered by timestamps)
     * @type {Array}
     */
    this.planetsKillList = []

    /**
     * Distance between planets.
     * @type {number}
     */
    this.spawnLineRadChangeFactor = 50

    /**
     * Radius of new planets spawn position.
     * @type {number}
     */
    this.spawnLineRadius = 0;//[px radius]
    this.lastSpawnTime = 0; //[ms]
    this.spawnInterval = -1; //[ms]

    this.planetProperties = {
        maxRadius: 20,
        minRadius: 11,
        maxShipsPotency: 7  //(max ships)^n   maxShipsPotency+1 <= n > 0
    }

    this.ProcessKillList = function () {
        if (this.planetsKillList.length != 0) {
            var date = Date.now()
            for (var i = 0; i < this.planetsKillList.length; i++) {
                if (date < this.planetsKillList[i].time)
                    break;
                io.sockets.in(this.id).emit('del planet', {planetId: this.planetsKillList[i].planetId})
                this.RebuildPlanetsCatch() //FIXME is error still here
                var planet_index = this.planetsCatch[this.planetsKillList[i].planetId]
                if (this.planets[planet_index] !== undefined) {
                    var owner_id = this.planets[planet_index].ownerId
                    if (owner_id != -1) {
                        this.players[owner_id].planetsCount--
                        this.IsPlayerDead(owner_id)
                    }
                    this.planets[planet_index].isGhost = true
                    this.planets.splice(planet_index, 1)
                } else {
                    console.warn("Planet index is undefined!")
                }
                this.RebuildPlanetsCatch()
                this.planetsKillList.splice(i, 1)
                i--
            }
        }
    }

    this.SpawnPlanets = function (deltaTime, force) {
        this.lastSpawnTime += deltaTime
        if (this.spawnInterval != -1 && this.lastSpawnTime > this.spawnInterval || force) {
            this.lastSpawnTime = 0
            var rotation = Math.random() * Math.PI * 2;
            var x = this.spawnLineRadius * Math.cos(rotation);
            var y = this.spawnLineRadius * Math.sin(rotation);
            var r = Math.floor((Math.random() * (this.planetProperties.maxRadius - this.planetProperties.minRadius)) + this.planetProperties.minRadius)
            var maxShipCapacity = Math.pow(2, 1 + Math.ceil(((r - this.planetProperties.minRadius) * this.planetProperties.maxShipsPotency) / (this.planetProperties.maxRadius - this.planetProperties.minRadius)))
            var planet = new Planet({
                r: r,
                color: "gray",
                ownerId: -1,
                x: x,
                y: y,
                ships: Math.ceil(maxShipCapacity / 2),
                maxShipCapacity: maxShipCapacity,
                id: ++rooms[roomId].nextPlanet, //set id of planet
                orbit: {
                    r: 0,
                    x: 0,
                    y: 0,
                    rotationAngle: 0
                }
            });
            planet.blackHole.pullSpeed = 0.0059
            planet.RecalculateDestination()

            var killTime = planet.GetTimeToReachDestination()
            if (!this.frozen)
                this.planetsKillList.push({time: killTime + Date.now(), planetId: planet.id})
            //console.debug(this.planetsKillList)
            this.planetsKillList.sort(function (a, b) {
                return a.time - b.time
            })

            return this.AddNewPlanet(planet)
            //console.debug("Planet added. New sys status: " + util.inspect(process.memoryUsage()));
        }
    }

    this.AddNewPlanet = function (planet) {
        var planet_index = this.planets.length
        this.planets.push(planet)
        this.planetsCatch[planet.id] = planet_index
        io.sockets.in(this.id).emit('rebuild', {
            planets: [planet]
        });
        return planet_index
    }

    /**
     * Array of catched planets.  { id }
     * @type {null}
     */
    this.planetsCatch = {}

    /**
     * Rebuilds planets catch
     * @constructor
     */
    this.RebuildPlanetsCatch = function () {
        this.planetsCatch = {};
        for (var i = 0; i < this.planets.length; i++) {
            this.planetsCatch[this.planets[i].id] = i;
        }
    }

    this.Unfroze = function () {
        this.frozen = false
        for (var i = 0; i < this.planets.length; i++) {
            var killTime = this.planets[i].GetTimeToReachDestination()
            this.planetsKillList.push({time: killTime + Date.now(), planetId: this.planets[i].id})
        }
        this.RebuildPlanetsCatch()
    }

    /**
     * Attacks planet id data is OK and returns ship id, else returns false.
     * @param ownerId
     * @param sourcesId
     * @param targetId
     * @returns {Number | Bool}
     * @constructor
     */
    this.AttackPlanet = function (ownerId, sourcesId, targetId) {

        for (var i = 0; i < sourcesId.length; i++) {
            var planetIndex = this.planetsCatch[sourcesId[i]];
            if (planetIndex !== undefined) {
                if (this.planets[planetIndex].ownerId == ownerId) {
                    //var shipIndex = this.ships.length
                    var shipId = ++this.nextShip
                    this.ships.push(new Ship({
                        x: this.planets[planetIndex].x,
                        y: this.planets[planetIndex].y,
                        ownerId: ownerId,
                        planetSourceId: this.planets[planetIndex].id,
                        id: shipId,
                        health: this.planets[planetIndex].ships
                    }).SetTarget(this.planets[this.planetsCatch[targetId]]));

                    this.planets[planetIndex].ships = 0
                    return shipId;
                }
            } else {
                console.debug(["WARNING unknown planet!", sourcesId[i], this.planets, this.planetsCatch]);
            }
        }
        return false;
    }

    /**
     * Returns players array ready to be send to clients (NOTE: socket id is removed for security reasons)
     * @returns { playerId: {name, color}}
     * @constructor
     */
    this.GetBasicPlayers = function () {   //FIXME roomID??
        var p = {}
        for (var key in this.players) {
            p[key] = {
                name: this.players[key].name,
                colorHsl: this.players[key].colorHsl
            }
        }
        return p
    }

    /**
     * Checks if player is dead by looking in each planet
     * @param playerId
     * @param ignoreShipId  - ignore ship id
     * @returns {boolean}
     * @constructor
     */
    this.HasPlayerShips = function (playerId, ignoreShipId) {   //NOT IN USE
        var shipsLength = this.ships.length
        for (var i = 0; i < shipsLength; i++) {
            if (this.ships[i].ownerId == playerId && (ignoreShipId === undefined
                || ignoreShipId != this.ships[i].id)
            ) {
                return true
            }
        }
        return false
    }

    /**
     * Trigger chek acction if player is dead
     * @param playerId
     * @param ignoreShipId - ignore specific ship when counting players ships
     * @constructor
     */
    this.IsPlayerDead = function (playerId, ignoreShipId) {
        if (playerId !== undefined && this.players[playerId] !== undefined) {
            if (playerId != -1 && this.players[playerId].planetsCount <= 0) {
                //need to check if any ships of this player my be in space traveling
                if (!this.HasPlayerShips(playerId, ignoreShipId)) {
                    console.debug("Player dead: " + this.id)
                    //delete this.players[playerId]
                    var v = this.CheckVictory()
                    io.sockets.in(this.id).emit('player dead', {playerId: playerId, victory: v})
                }
            }
        } else {
            console.warn("WARNING! Player id is UNDEFINED or player doesn't exist.")
        }
    }

    /**
     * Chek if player name exists
     * @param name
     * @returns {boolean} - true if name found
     * @constructor
     */
    this.PlayerNameExists = function (name) {
        for (var key in this.players) {
            if (this.players[key].name == name)
                return true
        }
        return false;
    }

    /**
     * Returns unique color based on "this.colorGen_start" seed.
     * @returns {hsl object}
     * @constructor
     */
    this.GetNextPlayerColor = function () {
        this.colorGen_start += 0.618033988749895
        this.colorGen_start %= 1
        return helpFun.HsvToHsl(rooms[roomId].colorGen_start, 0.5, 0.7)
    }

    /**
     * Set empty planet to new player. And return planet index.
     * @param playerId
     * @returns {undefined}
     * @constructor
     */
    this.SetEmptyPlanetToPlayer = function (playerId) { //FIXME
        var pLen = this.planets.length - 1
        if (pLen >= 0)
            this.spawnLineRadius = helpFun.CalculateDistance(0, 0, this.planets[pLen].x, this.planets[pLen].y)
        else
            this.spawnLineRadius = 250

        for (var i = 0; i < 4; i++) {
            this.spawnLineRadius += this.spawnLineRadChangeFactor
            this.SpawnPlanets(0, true)
        }

        this.spawnLineRadius += this.spawnLineRadChangeFactor
        var freePlanet = this.SpawnPlanets(0, true)

        for (var i = 0; i < 1; i++) {
            this.spawnLineRadius += this.spawnLineRadChangeFactor
            this.SpawnPlanets(0, true)
        }

        this.planets[freePlanet].ownerId = playerId
        this.planets[freePlanet].ships = 64
        this.players[playerId].planetsCount++

        return freePlanet
    }

    /**
     *
     * @returns {number} id of player who won or -1 if nobody yet won
     * @constructor
     */
    this.CheckVictory = function () {
        var playersIds = Object.keys(this.players)
        console.debug(playersIds)
        if (playersIds.length >= 1) {
            var notDeadCount = 0
            var notDeadId = null
            for (var pId in this.players) {
                if (this.players[pId].planetsCount > 0 || this.HasPlayerShips(pId)) {
                    notDeadCount++
                    notDeadId = pId
                }
            }
            if(notDeadCount == 1) {
                io.sockets.in(this.id).emit("win", {
                    playerId: notDeadId,
                    points: this.players[notDeadId].points
                });
                this.deleteMe = true
                return notDeadId
            } else if(notDeadCount == 0){
                this.deleteMe = true
            }
        } else if (playersIds.length == 0) {
            this.deleteMe = true
        }
        return -1
    }

    this.ClearPlayer = function (playerId) {
        console.debug("Clean player action called.")

        //remove all and emit change
        var pLen = this.planets.length

        for (var i = 0; i < pLen; i++) {
            if (this.planets[i].ownerId == playerId) {
                this.planets[i].ownerId = -1
                this.planets[i].color = 'gray'
            }
        }

        var sLen = this.ships.length
        for (var i = 0; i < sLen; i++) {
            if (this.ships[i].ownerId == playerId) {
                this.ships[i].ownerId = -1
                this.ships[i].color = 'gray'
            }
        }

        delete this.players[playerId]

        if (this.CheckVictory() == -1) {
            io.sockets.in(this.id).emit("rebuild", {
                planets: rooms[roomId].planets,
                players: rooms[roomId].GetBasicPlayers()
            });
        }
    }
}

/**
 * Make "public" folder visible
 */
app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'public/index.html'));
});

io.on('connection', function (socket) {

    socket.on('disconnect', function () {
        console.debug('User ' + socket.playerId + ' disconnected. Room: ' + socket.roomId);
        if (socket.roomId !== undefined && rooms[socket.roomId] !== undefined && rooms[socket.roomId].players[socket.playerId]) {
            io.sockets.in(socket.roomId).emit('chat msg', {
                isSysMsg: true,
                msg: "Player " + rooms[socket.roomId].players[socket.playerId].name + " left game."
            });
            rooms[socket.roomId].ClearPlayer(socket.playerId)
        }
    });

    socket.on('get rooms', function () {
        var r = []
        for (var id in rooms) {
            r.push({name: rooms[id].name, password: (rooms[id].password === null) ? false : true, id: id})
        }
        socket.emit("rooms", r);
    })

    socket.on('send peerId', function (data) {
        console.log("send peerId triggered")
        if (socket.roomId !== false && rooms[socket.roomId] !== undefined) {
            if (rooms[socket.roomId].password !== null) {
                rooms[socket.roomId].players[socket.playerId].webrtcID = data.id

                socket.broadcast.to(socket.roomId).emit("send peerId", {id: data.id, playerId: socket.playerId});
                console.log("send peerId: " + data.id)
            }
        }
    })

    socket.on('get peerId', function () {
        if (socket.roomId && rooms[socket.roomId] !== undefined) {
            if (rooms[socket.roomId].password !== null) {
                for (var id in rooms[socket.roomId].players) {
                    if (id != socket.playerId) {
                        if (rooms[socket.roomId].players[id].webrtcID !== null) {
                            socket.emit("send peerId", {id: rooms[socket.roomId].players[id].webrtcID, playerId: id});
                            console.log("get peerId: " + rooms[socket.roomId].players[id].webrtcID)
                        }
                        break
                    }
                }
            }
        }
    })

    socket.on('register', function (data) {


        var roomId = undefined
        var roomPassword = false
        var privateRoomSecondPlayer = false
        if (data.room === undefined) {
            if (data.private) {
                roomPassword = Math.floor(Math.random() * 890) + 105
                roomId = CreateRoom("Private " + Date.now(), 2, roomPassword);
            } else {
                roomId = FindRoom()
            }
        } else {
            if (rooms[data.room] !== undefined && rooms[data.room].password === null) {
                roomId = data.room
            } else if (rooms[data.room] !== undefined && rooms[data.room].password == data.password &&
                Object.keys(rooms[data.room].players).length == 1) {
                roomId = data.room
                rooms[data.room].Unfroze()
                privateRoomSecondPlayer = true
                socket.broadcast.to(rooms[roomId].id).emit("start game");
            } else {
                socket.emit("registered", {
                    successful: false,
                    error: "Can't connect to that room."
                });
                return
            }
        }

        if (data.name == "") {
            data.name = "Player " + (rooms[roomId].nextPlayerId + 1)
            var name = data.name
            for (var i = 99; i >= 0; i--) {
                if (!rooms[roomId].PlayerNameExists(name)) {
                    data.name = name
                    break;
                }
                name = data.name + i
                if (i == 0) {
                    socket.emit("registered", {
                        successful: false,
                        error: "We couldn't generate random name for you. Please enter player name."
                    });
                    return
                }
            }
        } else if (helpFun.IsValidName(data.name)) {
            if (rooms[roomId].PlayerNameExists(data.name)) {
                //PLAYER NAME ALREADY TAKEN
                socket.emit("registered", {
                    successful: false,
                    error: "Player name already taken."
                });
                return
            }
        } else {
            //INVALID NAME
            socket.emit("registered", {
                successful: false,
                error: "[SERVER] Invalid name."
            });
            return
        }

        socket.playerId = rooms[roomId].nextPlayerId++
        console.debug("User registered: " + data.name);

        if (roomPassword) {
            rooms[roomId].name = "Private " + data.name
            rooms[roomId].frozen = true
        }

        /**
         * Add user under room players list.
         */
        rooms[roomId].players[socket.playerId] = {
            sId: socket.id,
            colorHsl: rooms[roomId].GetNextPlayerColor(),
            name: data.name,
            planetsCount: 0,
            points: 0,
            webrtcID: null,
            reconnectHash: roomId + '-' + socket.playerId + '-' + Math.random().toString(36).substr(2) + new Date().getTime()
        };


        //Join user into room
        socket.join(roomId);
        socket.roomId = roomId

        var planet_index = rooms[roomId].SetEmptyPlanetToPlayer(socket.playerId)

        socket.emit("registered", {
            successful: true,
            playerId: socket.playerId,
            password: roomPassword,
            reconnectHash: rooms[roomId].players[socket.playerId].reconnectHash,
            privateRoomSecondPlayer: privateRoomSecondPlayer,
            rebuild: {
                players: rooms[roomId].GetBasicPlayers(),
                planets: rooms[roomId].planets,
                ships: rooms[roomId].ships,
                lastUpdate: GetLastUpdateTime()
            }
        });

        socket.broadcast.to(rooms[roomId].id).emit("rebuild", {
            planets: [rooms[roomId].planets[planet_index]],
            players: rooms[roomId].GetBasicPlayers()
        });

        socket.broadcast.to(rooms[roomId].id).emit("chat msg", {
            msg: "Player \"" + data.name + "\" joined game.",
            isSysMsg: true
        });
    });

    /**
     * { from (planet id), to(planet id)  } - Object
     */
    socket.on('attack planet', function (data) {
        if (socket.roomId !== undefined) {
            var shipId = rooms[socket.roomId].AttackPlanet(socket.playerId, data.sources, data.target)
            if (shipId !== false) {
                rooms[socket.roomId].evenetCounter++
                //socket.broadcast.to(rooms[socket.roomId].id).emit("attack planet", { ownerId: socket.playerId, sources: data.sources, target: data.target });
                io.sockets.in(socket.roomId).emit('attack planet', {
                    ownerId: socket.playerId,
                    sources: data.sources,
                    target: data.target,
                    shipId: shipId
                });
            } else {
                socket.emit("rebuild", {
                    successful: false,
                    clearPlanets: true,
                    planets: rooms[socket.roomId].planets
                });
            }
        }
    });

    /**
     * Ping - pong
     */
    socket.on('ping', function (data) {
        socket.emit('pong', data);
    });

    /**
     * Send requested stuff.
     */
    socket.on('rebuild', function (data) { //FIXME
        if (socket.roomId !== undefined) {
            if (!data.all) {
                console.debug("Player: " + socket.playerId + " requested rebuild")
                var response = {ships: [], planets: []}
                if (data.ships !== undefined) {
                    if (data.ships instanceof Array) {
                        for (var i = 0; i < data.ships.length; i++) {
                            rooms[socket.roomId]
                            //FIXME
                            response.ships.push()  //TODO ne pozabi še deltaTime !
                        }
                    } else {
                        //force rebuild
                        response.planets = rooms[socket.roomId].ships
                    }
                }
                if (data.planets !== undefined) {
                    if (data.planets instanceof Array) {
                        for (var i = 0; i < data.planets.length; i++) {

                            //rooms[socket.roomId].planets[]   //FIXME
                            response.planets.push()  //TODO ne pozabi še deltaTime !

                        }
                    } else {
                        //force rebuild
                        response.planets = rooms[socket.roomId].ships
                    }
                }
            }
        }
    })

    /**
     * Chat system.
     */
    socket.on('chat msg', function (data) {
        if (socket.roomId !== undefined && data.msg) {
            io.sockets.in(socket.roomId).emit('chat msg', {
                playerId: socket.playerId,
                msg: data.msg
            });
        }
    });
});

//Set interval for updating logic.
setInterval(Update, 250);

/**
 * Return elapsed milliseconds from last Update function trigger.
 * @returns {number}
 */
function GetLastUpdateTime() {
    return new Date().getTime() - lastRefreshTime;
};

//last time Update function was triggered.
var lastRefreshTime = 0;

/**
 * Update game roooms.
 */
function Update() {

    var now = new Date().getTime();
    var deltaTime = now - lastRefreshTime;
    lastRefreshTime = now;

    var roomLength = roomsCatch.length;
    for (var r = 0; r < roomLength; r++) {
        if (rooms[roomsCatch[r]].deleteMe) {
            console.debug("Deleted room " + rooms[roomsCatch[r]].id + ". New status:")
            delete rooms[roomsCatch[r]]
            console.debug(Object.keys(rooms))
            roomsCatch.splice(r, 1)
            roomLength--

        } else {

            if (rooms[roomsCatch[r]].frozen)
                continue

            //PLANETS
            var planetsLength = rooms[roomsCatch[r]].planets.length;
            for (var p = 0; p < planetsLength; p++) {
                rooms[roomsCatch[r]].planets[p].Update(deltaTime);
                rooms[roomsCatch[r]].planets[p].UpdateLogic(deltaTime);
            }

            //SHIPS
            var shipsLength = rooms[roomsCatch[r]].ships.length;
            for (var s = 0; s < shipsLength; s++) {
                if (rooms[roomsCatch[r]].ships[s].Update(deltaTime)) { //If ship reached target.

                    var result = rooms[roomsCatch[r]].ships[s].DestinationReachedAction()
                    if (result !== undefined) {
                        if (rooms[roomsCatch[r]].ships[s].ownerId != -1) {
                            rooms[roomsCatch[r]].players[rooms[roomsCatch[r]].ships[s].ownerId].planetsCount++
                            if (rooms[roomsCatch[r]].ships[s].planetTarget.ships < 5)
                                rooms[roomsCatch[r]].players[rooms[roomsCatch[r]].ships[s].ownerId].points += 15
                            else
                                rooms[roomsCatch[r]].players[rooms[roomsCatch[r]].ships[s].ownerId].points += 0.5
                        }
                        if (result != -1) {
                            rooms[roomsCatch[r]].players[result].planetsCount--
                            rooms[roomsCatch[r]].IsPlayerDead(result)
                        }
                    } else {
                        if (rooms[roomsCatch[r]].ships[s].ownerId != -1)
                            rooms[roomsCatch[r]].IsPlayerDead(rooms[roomsCatch[r]].ships[s].ownerId, rooms[roomsCatch[r]].ships[s].id)
                    }

                    var planetsToRebuild = []
                    if(!rooms[roomsCatch[r]].ships[s].planetTarget.isGhost){
                        planetsToRebuild.push(rooms[roomsCatch[r]].ships[s].planetTarget)
                    }

                    //TODO [not critical] no need for rebuilding whole planet
                    console.debug("del ship " + rooms[roomsCatch[r]].ships[s].id);
                    io.sockets.in(rooms[roomsCatch[r]].id).emit('del ship', {
                        shipId: rooms[roomsCatch[r]].ships[s].id,
                        rebuild: {planets: planetsToRebuild}
                    });

                    rooms[roomsCatch[r]].ships.splice(s, 1)
                    s--
                    shipsLength--
                }
            }

            //Destroy planets in black hole.
            rooms[roomsCatch[r]].ProcessKillList()

            //Create new planets.
            rooms[roomsCatch[r]].SpawnPlanets(deltaTime)
        }
    }
};

var port = (!isNaN(process.argv.slice(2)[0])) ? Number(process.argv.slice(2)[0]) : 3000
http.listen(port, function () {
    console.log('Running on port: ' + port + ((debug) ? " - /!\\ Debug mode is ON /!\\\n" : "" ));
});