module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        copy: {
            prepare_index: {
                files: [
                    {
                        expand: true,
                        filter: 'isFile',
                        src: 'public/dev.html',
                        dest: '',
                        rename: function(dest, src) {
                            return dest + src.replace('dev','index');
                        }
                    }
                ]
            },
            server:{
                files: [
                    {
                        filter: 'isFile',
                        src: 'src/g.ship.js',
                        dest: 'server/Ship.js'
                    },
                    {

                        filter: 'isFile',
                        src: 'src/g.planet.js',
                        dest: 'server/Planet.js'
                    },
                    {

                        filter: 'isFile',
                        src: 'src/g.helperFun.js',
                        dest: 'server/helperFun.js'
                    }
                ]
            },
            'client-dev':{
                files: [
                    {
                        filter: 'isFile',
                        src: 'src/g.ship.js',
                        dest: 'public/dev/g.ship.js'
                    },
                    {
                        filter: 'isFile',
                        src: 'src/g.planet.js',
                        dest: 'public/dev/g.planet.js'
                    },
                    {
                        filter: 'isFile',
                        src: 'src/g.helperFun.js',
                        dest: 'public/dev/g.helperFun.js'
                    }
                ]
            }
        },
        jsdoc: {
            dist: {
                src: ['src/g.*.js'],
                options: {
                    destination: 'doc'
                }
            }
        },
        useminPrepare: {
            html: 'public/dev.html',
            options: {
                dest: 'public'
            }
        },
        usemin: {
            html: ['public/index.html']
        },
        strip_code: {
            server: {
                options: {
                    start_comment: 'start-client-block',
                    end_comment: 'end-client-block'
                },
                files: [
                    { src: 'server/*' },
                ]
            },
            client: {
                options: {
                    start_comment: 'start-server-block',
                    end_comment: 'end-server-block'
                },
                files: [
                    { src: 'public/dev/*.js'},
                ]
            }
        },
        watch: {
            dev: {
                files: ['src/g.planet.js', 'src/g.ship.js', 'src/g.helperFun.js'],
                tasks: ['server', 'client-dev']
            },
            html: {
                files: ['public/dev.html'],
                tasks: ['build']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-jsdoc');
    grunt.loadNpmTasks('grunt-strip-code');

    grunt.registerTask('default', [
        'server', 'client-dev', 'build'
    ]);

    grunt.registerTask('server', [
        'copy:server',
        'strip_code:server'
    ]);
    grunt.registerTask('client-dev', [
        'copy:client-dev',
        'strip_code:client'
    ]);

    grunt.registerTask('build', [
        'copy:prepare_index',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'usemin',
        //'jsdoc'
    ]);

};