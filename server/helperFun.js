/**
 * @file Helper functions for server and client side.
 */

var game = (function (g) {
    "use strict";

    var h = {}

    /**
     * Calculate distance between two points.
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @returns {number}
     * @constructor
     */
    h.CalculateDistance = function(x1,y1,x2,y2){//NOT IN USE
        return Math.sqrt( Math.pow(x1-x2, 2) + Math.pow(y1-y2, 2))
    }

    /**
     * Convert hsl object to html value string.
     * @param colorHsl
     * @returns {string}
     * @constructor
     */
    h.HslToString = function(colorHsl){
        return 'hsl('+colorHsl.h +','+colorHsl.l+'%,'+colorHsl.s+'%)'
    }

    /**
     * If player name is valid name.
     * @param name
     * @returns {boolean}
     * @constructor
     */
    h.IsValidName = function (name) {
        var len = name.length
        if(len < 3 || len > 12)
            return false;
        return /^[A-Za-z0-9 _]*$/.test(name)
    }

    /**
     * Get dot product.
     * @param dx1
     * @param dy1
     * @param dx2
     * @param dy2
     * @returns {number}
     */
    h.dotproduct = function (dx1, dy1, dx2, dy2) {
        return (dx1 * dx2) + (dy1 * dy2)
    }

    /**
     * Calculate length.
     * @param x
     * @param y
     * @returns {number}
     */
    h.len = function (x, y) {
        return Math.sqrt(x * x + y * y);
    }

    /**
     * Convert hsv value to hsl object.
     * @param h
     * @param s
     * @param v
     * @returns {Array|*}
     * @constructor
     */
    h.HsvToHsl = function (h, s, v) { //FIXME
        var rgb = this.HSVtoRGB(h, s, v)
        return this.rgbToHsl(rgb.r, rgb.g, rgb.b)
    }

    //hidden Use: HsvToHsl
    h.hsv2hsl = function (h, s, v) {
        var hh = h;
        var ll = (2 - s) * v;
        var ss = s * v;
        ss /= (ll <= 1) ? (ll) : 2 - (ll);
        ll /= 2;

        return {
            h: hh,
            s: ss,
            l: ll
        }
    }

    /**
     * Converts an RGB color value to HSL. Conversion formula
     * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
     * Assumes r, g, and b are contained in the set [0, 255] and
     * returns h, s, and l in the set [0, 1].
     *
     * @param   Number  r       The red color value
     * @param   Number  g       The green color value
     * @param   Number  b       The blue color value
     * @return  Array           The HSL representation
     */
    h.rgbToHsl = function (r, g, b) {
        r /= 255, g /= 255, b /= 255;
        var max = Math.max(r, g, b), min = Math.min(r, g, b);
        var h, s, l = (max + min) / 2;

        if (max == min) {
            h = s = 0; // achromatic
        } else {
            var d = max - min;
            s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
            switch (max) {
                case r:
                    h = (g - b) / d;
                    break;
                case g:
                    h = 2 + ( (b - r) / d);
                    break;
                case b:
                    h = 4 + ( (r - g) / d);
                    break;
            }
            h *= 60;
            if (h < 0) h += 360;
        }
        return {h: h, s: s * 100, l: l * 100};
    }

    /* accepts parameters
     * h  Object = {h:x, s:y, v:z}
     * OR
     * h, s, v
     */
    h.HSVtoRGB = function (h, s, v) {
        var r, g, b, i, f, p, q, t;
        if (h && s === undefined && v === undefined) {
            s = h.s, v = h.v, h = h.h;
        }
        i = Math.floor(h * 6);
        f = h * 6 - i;
        p = v * (1 - s);
        q = v * (1 - f * s);
        t = v * (1 - (1 - f) * s);
        switch (i % 6) {
            case 0:
                r = v, g = t, b = p;
                break;
            case 1:
                r = q, g = v, b = p;
                break;
            case 2:
                r = p, g = v, b = t;
                break;
            case 3:
                r = p, g = q, b = v;
                break;
            case 4:
                r = t, g = p, b = v;
                break;
            case 5:
                r = v, g = p, b = q;
                break;
        }
        return {
            r: Math.floor(r * 255),
            g: Math.floor(g * 255),
            b: Math.floor(b * 255)
        };
    }

    g.helpFun = h

    return g;
}(game || {}));

/* start-server-block */
//show this to node.js as module
module.exports = game.helpFun;
/* end-server-block */