/**
 * @file engine
 * @author Tilen Tomakić
 */

var game = (function (g) {
    "use strict";

    g.models = g.models || {};

    g.models.Ship = function (data) {

        /**
         * X position of ship in 2D space
         * @type {number}
         */
        this.x = 0;

        /**
         * Y position of ship in 2D space
         * @type {number}
         */
        this.y = 0;

        /**
         * Speed of ship.
         * @type {number}
         */
        this.speed = 0.09;

        /**
         * Unique number of ship
         * @type {undefined}
         */
        this.id = undefined

        /**
         * Color of ship. If in game set to "RED" means that something went wrong (client system will try to rebuild ship)
         * @type {string}
         */
        this.color = 'RED';

        /**
         * Rotation angle of ship in 2D space  (radians)
         * @type {number}
         */
        this.rotation = 0;

        /**
         * Health of ship. It can also mean how many ships are joined together into one ship.
         * @type {number}
         */
        this.health = 1;

        /**
         * Height of ship.
         * @type {number}
         */
        this.height = 20

        /**
         * Width of ship.
         * @type {number}
         */
        this.width = 0.3

        /**
         * Planet that ship should "attack" go in.
         * You shuldn't set this variable directly, use g.ship.SetTarget function instead.
         * @type {Planet object} - Should be planet object. Warning planet should not be deleted or ship will go into ghost.
         */
        this.planetTarget = undefined; // planet





        this.targetPos = {
            x: 0,
            y: 0
        }

        this.ownerId = undefined

        /**
         * Sets planet target for ship and rotates it in right direction.
         * (Currently function assumes that planets are static)
         * @param planet {Planet object}
         * @returns {g.models.Ship}
         * @constructor
         */
        this.SetTarget = function(planet){
            try {
                this.planetTarget = planet;
                this.CalculateDestinationPoint()
                this.rotation = Math.atan2(this.y - this.targetPos.y, this.x - this.targetPos.x);
            } catch (e){
                console.warn("PlanetTarget is undefined!")
                console.debug(e)
            }
            return this;
        }

        var dotproduct = function (dx1, dy1, dx2, dy2) {
            return (dx1 * dx2) + (dy1 * dy2)
        }
        var len = function (x, y) {
            return Math.sqrt(x * x + y * y);
        }

        this.CalculateDestinationPoint = function(){
            //Vector totarget =  target.position - tower.position;
            var totargetX = this.planetTarget.x - this.x
            var totargetY = this.planetTarget.y - this.y

            var targetVelocity = this.planetTarget.GetVelocity()

            //float a = Vector.Dot(target.velocity, target.velocity) - (bullet.velocity * bullet.velocity);
            var a = dotproduct(targetVelocity.x, targetVelocity.y,targetVelocity.x, targetVelocity.y) - (this.speed * this.speed)

            //float b = 2 * Vector.Dot(target.velocity, totargetX, totargetY);
            var b = 2 * dotproduct(targetVelocity.x, targetVelocity.y, totargetX, totargetY)

            //float c = Vector.Dot(totarget, totarget);
            var c = dotproduct(totargetX, totargetY, totargetX, totargetY)

            var p = -b / (2 * a);
            var q = Math.sqrt((b * b) - 4 * a * c) / (2 * a);

            var t1 = p - q;
            var t2 = p + q;
            var t;

            if (t1 > t2 && t2 > 0)
            {
                t = t2;
            }
            else
            {
                t = t1;
            }

            //Vector aimSpot = target.position + target.velocity * t;
            var aimSpotX = this.planetTarget.x + targetVelocity.x * t;
            var aimSpotY = this.planetTarget.y + targetVelocity.y * t;

            //Vector bulletPath = aimSpot - tower.position;
            var bulletPathX = aimSpotX - this.x;
            var bulletPathY = aimSpotY - this.y;

            var timeToImpact = len(bulletPathX, bulletPathY) / this.speed;//speed must be in units per second*/

            //console.debug("aimSpot: x"+aimSpotX + ' y' + aimSpotY)
            //console.debug("bulletPath: x"+bulletPathX + ' y' + bulletPathY)
            //console.debug("t"+timeToImpact)

            //test code:
            //game.particles.push( new game.models.Particle(1, bulletPathX, bulletPathY, "RED") )
            //game.particles.push( new game.models.Particle(1, aimSpotX, aimSpotY , "BLUE") )
            //game.particles.push( new game.models.Particle(1, this.planetTarget.x, this.planetTarget.y , "ORANGE") )

            this.targetPos.x = aimSpotX
            this.targetPos.y = aimSpotY

            return this
        }

        /**
         * Sets settings to the ship and returns this instance.
         * @param data
         * @returns {g.models.Ship}
         * @constructor
         */
        this.Set = function (data) {
            for (var index in data) {
                if (data.hasOwnProperty(index)) {

                    if (data[index] !== null && typeof data[index] === 'object') {
                        for (var index2 in data[index]) {
                            this[index][index2] = data[index][index2];

                        }
                    } else {
                        this[index] = data[index];
                    }
                }
            }
            return this;
        }



        /**
         * Update ship
         * @param gameTime - time passed from last update
         * @returns {*}
         * @constructor
         */
        this.Update = function (gameTime) {
            if(this.planetTarget !== undefined && !this.isGhost) {
                var oldOffsetX = this.x - this.targetPos.x
                var oldOffsetY = this.y - this.targetPos.y

                //this.rotation = Math.atan2(this.y - this.planetTarget.y, this.x - this.planetTarget.x);
                //this.rotation = Math.atan2(this.y - this.planetTarget.y, this.x - this.planetTarget.x);

                this.x -= this.speed * gameTime * Math.cos(this.rotation);
                this.y -= this.speed * gameTime * Math.sin(this.rotation);

                var newOffsetX = this.x - this.targetPos.x
                var newOffsetY = this.y - this.targetPos.y

                if( (oldOffsetX < 0) && (newOffsetX > 0) || (oldOffsetX > 0) && (newOffsetX < 0) ||
                    (oldOffsetY < 0) && (newOffsetY > 0) || (oldOffsetY > 0) && (newOffsetY < 0)
                ) {

                    return true
                }
            }

            //TODO
            if(g.shipsParticleTrail) {
                game.particleHelper.Explode(1, this.x, this.y, 0, 0, 0, 0, {color: this.color, shrink: 0.0015})
            }
            return false
        };

        //NOT IN USE
        this.UpdateLogic = function (gameTime) {

        }



        /**
         * Takes appropriate action when ship reached planet target.
         * (attacks planet (decreases ships count of planet) or increases ships count of planet)
         * @returns {undefined | number} - returnes undefined if owner of planet was not changed else returnes ownerid (player id)
         * @constructor
         */
        this.DestinationReachedAction = function(){
            if(this.planetTarget.ownerId == this.ownerId){
                this.planetTarget.ships += Math.ceil(this.health);
            } else {
                this.planetTarget.ships -= Math.ceil(this.health);
                if (this.planetTarget.ships < 0) {
                    this.planetTarget.ships = -this.planetTarget.ships
                    var oldOwnerId = this.planetTarget.ownerId;
                    this.planetTarget.ownerId = this.ownerId;
                    return oldOwnerId
                }
            }
            return undefined;
        }

        //initialise if parameters passed into
        if (data !== undefined) {
            this.Set(data);
        }
    };

    return g;
}(game || {}));

/* start-server-block */
//show this to node.js as module
module.exports = game.models.Ship;
/* end-server-block */