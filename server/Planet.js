/**
 * @file engine
 * @author Tilen Tomakić
 */

var game = (function (g) {
    "use strict";

    g.models = g.models || {};

    g.models.Planet = function (data) {

        /**
         * Planet radius
         * @type {number}
         */
        this.r = 10;

        //this.animateR = 0;

        /**
         * Unique number of planet.
         * @type {number}
         */
        this.id = undefined;

        /**
         * X position of planet (if orbit is set this position will be recalculated automatically)
         * @type {number}
         */
        this.x = 0;

        /**
         * Y position of planet (if orbit is set this position will be recalculated automatically)
         * @type {number}
         */
        this.y = 0;

        /**
         * Speed of moving planet (px/ms)
         * @type {number}
         */
        this.speed = 0.00021;



        /**
         *
         * @type {{r: number, rotationAngle: number, x: number, y: number}}
         */
        this.orbit = {
            r: 0,             //radius
            rotationAngle: 0, //angle of rotation (this determinants x and y position of planet)  [in radians]
            x: 0,             // center point of rotation for x
            y: 0              // center point of rotation for y
        };

        /**
         * Black hole object. If planet is being pulled into black hole (where, with what speed)
         * @type {{x: number, y: number, pullSpeed: number, velocity: {x: number, y: number}}}
         */
        this.blackHole = {
            x: 0,
            y: 0,
            pullSpeed: 0,
            velocity: {
                x: 0,
                y: 0
            }
        }

        /**
         * Ships that planet has
         * @type {number}
         */
        this.ships = 0;

        /**
         * Last time ship was generated
         * @type {number}
         */
        this.lastShipGenTime = 0;

        /**
         * Time that takes for 1 ship to generate
         * @type {number}
         */
        this.shipGenTime = 1000;

        /**
         * Maksimal numer of ships that can be generated on this planet.
         * ! This doesn't mean that ship from other friendly planet can't be transferred into this planet. Even if maxShipCapacity is reached.
         * @type {number}
         */
        this.maxShipCapacity = 64

        /**
         * @type {number} - ID of player, if -1 no player has this planet
         */
        this.ownerId = -1;









        /**
         * If planet can be used by players or is passive.
         * @type {boolean}
         */
        this.isPassive = false;

        /**
         * Array of functions that should be triggered after planet has updated. Can be left undefined.
         * @type {functions list}
         */
        this.onUpdateEvents = undefined;

        /**
         * If planet should be updated from main update loop.
         *
         * It's set to false if planet is orbiting around other planet.
         * In that case other planet will trigger update if has onUpdateEvents variable set.
         * @type {boolean}
         */
        this.autoUpdate = true;

        /**
         * Set settings for planet and return planet instance.
         * @param data
         * @returns {g.models.Planet}
         * @constructor
         */
        this.Set = function (data) {
            //console.log(data);
            for (var index in data) {
                if (data.hasOwnProperty(index)) {

                    if (data[index] !== null && typeof data[index] === 'object') {
                        for (var index2 in data[index]) {
                            this[index][index2] = data[index][index2];
                        }
                    } else {
                        this[index] = data[index];
                    }
                }
            }
            return this;
        };

        /**
         * Same as "Set" function. Only that this will set only settings that are legal.
         * @param data
         * @returns {g.models.Planet}
         * @constructor
         */
        this.SafeSet = function (data) {
            //console.log(data);
            for (var index in data) {
                if (data.hasOwnProperty(index)) {
                    if (data[index] !== null && typeof data[index] === 'object') {
                        switch (index) {
                            case 'orbit':
                                this.orbit = data[index];
                                break;
                            case 'blackHole':
                                this.blackHole = data[index];
                                break;
                        }
                    } else {
                        this[index] = data[index];
                    }
                }
            }
            return this;
        };

        /**
         *  Returns velocity with witch planet is being pulled into a black hole.
         * @returns {{x: number, y: number}}
         * @constructor
         */
        this.GetVelocity = function () {
            var rotation = Math.atan2(this.y - this.blackHole.y, this.x - this.blackHole.x);
            var x = -this.blackHole.pullSpeed * Math.cos(rotation);
            var y = -this.blackHole.pullSpeed * Math.sin(rotation);
            return {x: x, y: y}
        };

        /**
         *
         * @returns {g.models.Planet}
         * @constructor
         */
        this.RecalculateDestination = function () {
            var rotation = Math.atan2(this.y - this.blackHole.y, this.x - this.blackHole.x);
            this.blackHole.velocity.x = this.blackHole.pullSpeed * Math.cos(rotation);
            this.blackHole.velocity.y = this.blackHole.pullSpeed * Math.sin(rotation);
            return this
        };

        /**
         * Returns time in witch planet will reach blackHole.
         * @returns {number}
         * @constructor
         */
        this.GetTimeToReachDestination = function () {
            //var dest = g.helpFun.CalculateDistance(this.y, this.blackHole.y, this.x, this.blackHole.x)
            var dest = Math.sqrt(Math.pow(this.y - this.blackHole.y, 2) + Math.pow(this.x - this.blackHole.x, 2))
            return dest * (1 / this.blackHole.pullSpeed); //[ms]
        };



        /**
         * Set planet to orbit around.
         * @param planet
         * @returns {g.models.Planet}
         * @constructor
         */
        this.OrbitAroundPlanet = function (planet) {
            this.orbitAroundPlanetId = planet.id;
            this.autoUpdate = false;
            var p = this;
            planet.onUpdate(function (data) {
                p.orbit.x = data.this.x;
                p.orbit.y = data.this.y;
                p.Update(data.gameTime);
            });
            return this;
        };

        /**
         * Set function to trigger after planet has updated.
         * @param event
         */
        this.onUpdate = function (event) {
            if (this.onUpdateEvents === undefined) {
                this.onUpdateEvents = [];
            }
            this.onUpdateEvents.push(event);
        };

        /**
         * Triggers functions stored functions inside onUpdateEvents after update has finished.
         * @param gameTime
         * @constructor
         */
        this.TriggerUpdateEvents = function (gameTime) {
            if (this.onUpdateEvents !== undefined) {
                for (var i = 0; i < this.onUpdateEvents.length; i++) {
                    this.onUpdateEvents[i]({'this': this, gameTime: gameTime});
                }
            }
        };

        /**
         * Update planet.
         * @param deltaTime
         * @constructor
         */
        this.Update = function (deltaTime) {
            //MOVE around orbit
            if (this.blackHole.pullSpeed != 0) {
                this.x -= this.blackHole.velocity.x * deltaTime
                this.y -= this.blackHole.velocity.y * deltaTime
            }
            else if (this.orbit.r !== 0) {
                this.orbit.rotationAngle += this.speed * deltaTime;

                this.x = Math.cos(this.orbit.rotationAngle) * this.orbit.r + this.orbit.x;
                this.y = Math.sin(this.orbit.rotationAngle) * this.orbit.r + this.orbit.y;
            }
            /*if(this.animateR != 0){
                this.animateR -= 0.0007  * deltaTime
                console.log(this.animateR)
                if(this.animateR < 0)
                    this.animateR = 0
            }*/
            this.TriggerUpdateEvents(deltaTime);
        };

        /**
         * Returns true if given point (x,y) is inside planet on a 2D space
         * @param x
         * @param y
         * @returns {boolean}
         * @constructor
         */
        this.PointIntersects = function (x, y) {
            return Math.pow(x - this.x, 2) + Math.pow(y - this.y, 2) <= Math.pow(this.r, 2);
        };

        /**
         * Updates planet, with not so important stuff.
         * @param gameTime
         * @constructor
         */
        this.UpdateLogic = function (gameTime) {

            this.orbit.rotationAngle -= ~~(this.orbit.rotationAngle / (Math.PI * 2)) * (Math.PI * 2);

            if (this.maxShipCapacity > this.ships && !this.isPassive) {
                this.lastShipGenTime += gameTime;
                if (this.lastShipGenTime > this.shipGenTime) {
                    var i = ~~(this.lastShipGenTime / this.shipGenTime);

                    this.ships += i;
                    this.lastShipGenTime -= i * this.shipGenTime;

                    if (this.maxShipCapacity <= this.ships) {
                        this.lastShipGenTime = 0
                        this.ships = this.maxShipCapacity
                    }
                }
            }


        };





        //initialise if parameters passed into
        if (data !== undefined) {
            this.SafeSet(data);
        }
    };

    return g;
}(game || {}));

/* start-server-block */
//show this to node.js as module
module.exports = game.models.Planet;
/* end-server-block */