function TestObj(){

    this.num = 0
    this.DoSomething = function(){
        this.num++
    }
}

function _array_test(len){
    var a = []

    for(var x = 0; x < len; x++) {
        a.push(new TestObj())
    }

    t = new Date().getTime()
    //clasicčni loop
    for(var l = 0; l < loops; l++){
        for(var i = 0; i < a.length; i++) {
            a[i].DoSomething()
        }
    }
}
function _array_test2(len){
    var a = []

    for(var x = 0; x < len; x++) {
        a.push(new TestObj())
    }

    t = new Date().getTime()
    //s predshranjeno dolžino
    var alen = a.length
    for(var l = 0; l < loops; l++){
        for(var i = 0; i < alen; i++) {
            a[i].DoSomething()
        }
    }
}
function _object_test(len){
    var a = {}
    var t = []
    for(var x = 0; x < len; x++) {
        a[x+2] = new TestObj()
        t.push(x+2)
    }

    t = new Date().getTime()
    //s predshranjeno dolžino
    var alen = t.length
    for(var l = 0; l < loops; l++){
        for(var i = 0; i < alen; i++) {
            a[t[i]].DoSomething()
        }
    }
}

function _object_test2(len){
    var a = {}

    for(var x = 0; x < len; x++) {
        a[x+2] = new TestObj()
    }

    t = new Date().getTime()
    for(var l = 0; l < loops; l++){
        for(var k in a) {
            a[k].DoSomething()
        }
    }
}

var len = 300
var loops = 500000


_array_test(len)
console.log((new Date().getTime() - t)/loops)


_array_test2(len)
console.log((new Date().getTime() - t)/loops)


_object_test(len)
console.log((new Date().getTime() - t)/loops)

_object_test2(len)
console.log((new Date().getTime() - t)/loops)
