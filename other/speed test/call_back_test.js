var len = 2
var loops = 500000

a = []

function solutionA(){
    a = []
    for(var x = 0; x < len; x++) {
        a[x] = { a : function(){
            return true;
        } }
    }

    t = new Date().getTime()
    for(var i = 0; i < a.length; i++) {
        if(a[i].a()) {
            a.splice(i, 1)
            i--
        }
    }

    console.log(a)
}

function solutionB(){
    a = []
    for(var x = 0; x < len; x++) {
        var y = x
        a[x] = { a : function(){
            this.c(y)
        }, c: function(y){
            console.log(y)
            a.splice(y, 1)
        } }
    }

    t = new Date().getTime()
    for(var i = 0; i < a.length; i++) {
        a[i].a()
    }

    console.log(a)
}

solutionA()
console.log((new Date().getTime() - t)/loops)
solutionB()
console.log((new Date().getTime() - t)/loops)