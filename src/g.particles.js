/**
 * @file engine
 * @author Tilen Tomakić
 */

var game = (function (g) {
    "use strict";

    g.models = g.models || {};

    /**
     * Create particle.
     * @param type
     * @param y
     * @param x
     * @constructor
     */
    g.models.Particle = function (x, y) {

        /**
         * Particle radius
         * @type {number}
         */
        this.r = 3;

        /**
         * X position of particle
         * @type {number}
         */
        this.x = 0;

        /**
         * Y position of particle
         * @type {number}
         */
        this.y = 0;

        /**
         * Speed of moving particle (px/ms)
         * @type {number}
         */
        this.velocity = {
            x: 0,
            y: 0
        };

        /**
         * Color of planet
         * @type {color}
         */
        this.color = 'RED';

        this.opacity = 1

        this.fadeOut = 0
        this.shrink = 0

        /**
         * Update particle.
         * @param deltaTime
         * @constructor
         */
        this.Update = function (deltaTime) {
            if (this.velocity.x && this.velocity.y) {
                this.x -= this.velocity.x * deltaTime
                this.y -= this.velocity.y * deltaTime
            }
            if(this.shrink){
                this.r -= this.shrink * deltaTime
                if(this.r <= 0)
                    return true
            }
            if(this.fadeOut){
                this.opacity -= this.fadeOut * deltaTime
                if(this.opacity <= 0){
                    return true
                }
            }
        };

        /**
         * Draw planet on canvas.
         * @constructor
         */
        this.Draw = function(){
            g._draw.ctx2.beginPath();

            g._draw.ctx2.arc((this.x + g.camera.x) * g.camera.zoom, (this.y + g.camera.y) * g.camera.zoom, this.r * g.camera.zoom, 0, Math.PI*2, true);
            g._draw.ctx2.fillStyle = this.color;

            g._draw.ctx2.globalAlpha = this.opacity
            g._draw.ctx2.fill();

            g._draw.ctx2.closePath();
        };

        /**
         * Sets settings to the ship and returns this instance.
         * @param data
         * @returns {g.models.Ship}
         * @constructor
         */
        this.Set = function (data) {
            for (var index in data) {
                if (data.hasOwnProperty(index)) {

                    if (data[index] !== null && typeof data[index] === 'object') {
                        for (var index2 in data[index]) {
                            this[index][index2] = data[index][index2];

                        }
                    } else {
                        this[index] = data[index];
                    }
                }
            }
            return this;
        }

        //initialise if parameters passed into
        if (!isNaN(x) && !isNaN(y)) {
            this.x = x
            this.y = y
        }
    };

    var particleHelper = {}

    /**
     * USAGE: game.particleHelper.Explode(4000,-500,-300,0.0001,0.25,undefined,undefined, {color:'rgba(80,10,60,0.4)'})
     * @param particles
     * @param x
     * @param y
     * @param fromSpeed
     * @param toSpeed
     * @param fromAngle
     * @param toAngle
     * @param color
     */
    particleHelper.Explode = function(particles, x, y, fromSpeed, toSpeed, fromAngle, toAngle, properties, forcedraw){

        if(!g.drawDetails && forcedraw !== true)
            return

        if(!fromAngle)
            fromAngle = 0

        if(!toAngle)
            toAngle = Math.PI * 2

        for(var i = 0; i < particles; i++){

            var speed = (!toSpeed) ? (fromSpeed || 0.008) : Math.random() * toSpeed + fromSpeed;

            var rotation = Math.random() * toAngle + fromAngle

            var vX = speed * Math.cos(rotation);
            var vY = speed * Math.sin(rotation);

            g.particles.push(new g.models.Particle(x, y).Set({ velocity: { x: vX, y: vY }}).Set(properties))
        }
    }

    /**
     * Clear particles.
     */
    particleHelper.Clear = function(){
        g.particles = []
    }

    g.particleHelper = particleHelper

    return g;
}(game || {}));