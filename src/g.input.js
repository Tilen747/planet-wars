/**
 * @file core
 * @author Tilen Tomakić
 */

var game = (function (g) {
    "use strict";

    var input = {};

    /**
     * Html element on which to listen for events.
     * @type {DOM element}
     */
    input.container = null;

    input.touch = undefined;

    /**
     * Initialize touch and mouse events.
     */
    input.Init = function () {

        input.container = document.getElementById("ingame_surface_container")

        //MOUSE & TOUCH
        input.container.onmousemove = input.onmousemove;
        input.container.onmousedown = input.onmousedown;
        input.container.onmouseup = input.onmouseup;
        input.container.onclick = input.onclick;

        input.container.onmousewheel = input.onmousewheel;

        input.touchPenMoveOrigCameraPos = {x: 0, y: 0}

        //TOUCH
        this.touch = new Hammer(input.container);
        this.touch.add(new Hammer.Pan({threshold: 0, pointers: 0})).recognizeWith([this.touch.get('pinch')]);
        this.touch.on('panstart panmove', function (e) {

            if(e.pointers.length!=1)
                return

            if (e.type == "panstart") {
                input.touchPenMoveOrigCameraPos = {x: g.camera.x, y: g.camera.y}
                return
            }

            g.camera.x = input.touchPenMoveOrigCameraPos.x + e.deltaX / g.camera.zoom //(e.velocityX * 4) / g.camera.zoom;
            g.camera.y = input.touchPenMoveOrigCameraPos.y + e.deltaY / g.camera.zoom//(e.velocityY * 4) / g.camera.zoom;

            g.camera.changed = true
        });

        input.touchPinchLastScale = 0
        this.touch.add(new Hammer.Pinch({threshold: 0})).recognizeWith([this.touch.get('pan')]);
        this.touch.on("pinchstart pinchmove", function (e) {

            if (e.type == "pinchstart") {
                input.touchPinchLastScale = e.scale
                return
            }

            var mX = e.center.x
            var mY = e.center.y

            var f = (e.scale - input.touchPinchLastScale)
            input.touchPinchLastScale = e.scale
            var k = (g.camera.zoom * f + g.camera.zoom * g.camera.zoom);

            g.camera.x = g.camera.x - ( (mX * f) / k)// - e.velocityX
            g.camera.y = g.camera.y - ( (mY * f) / k)// - e.velocityY

            g.camera.zoom += f;

            input.touchPenMoveOrigCameraPos = {x: g.camera.x, y: g.camera.y}
            g.camera.changed = true
        });
    };

    /**
     * On mouse scroll event. Change camera zoom.
     * @param e
     */
    input.onmousewheel = function (e) {  //TODO use function FocusCameraTo

        var f = e.wheelDeltaY * 0.001;
        var k = (g.camera.zoom * f + g.camera.zoom * g.camera.zoom);

        var oldCamX = g.camera.x
        var oldCamY = g.camera.y

        g.camera.x = g.camera.x - ( (e.offsetX * f) / k);
        g.camera.y = g.camera.y - ( (e.offsetY * f) / k);
        g.camera.zoom += f;

        if (g.camera.zoom < 0.1) {
            g.camera.zoom = 0.1;
            g.camera.y = oldCamY
            g.camera.x = oldCamX
        }

        g.camera.changed = true
    };

    //DRAG functionality

    input._ismouseDown = false;
    input._lastMousePos = null;

    input.onmousedown = function (e) {

        input._ismouseDown = true;
        input.container.style.cursor = 'move';
        if (g.todo) {
            g.HandleTodo()
        }
    };

    input.onmouseup = function (e) {

        input._ismouseDown = false;
        input._lastMousePos = null;
        input.container.style.cursor = 'default';
    };


    input.onmousemove = function (e) {
        if (input._ismouseDown) {
            var newPos = {x: e.offsetX, y: e.offsetY};
            if (input._lastMousePos !== null) {
                g.camera.x += (newPos.x - input._lastMousePos.x) / g.camera.zoom;
                g.camera.y += (newPos.y - input._lastMousePos.y) / g.camera.zoom;
            }
            input._lastMousePos = newPos;
            g.camera.changed = true
        }
    };

    /**
     * On click
     * @param e
     */
    input.onclick = function (e) {

        for (var i = 0; i < g.planets.length; i++) {
            if (g.planets[i].PointIntersects((e.offsetX / g.camera.zoom) - g.camera.x, (e.offsetY / g.camera.zoom) - g.camera.y)) {
                g.SelectPlanet(i);
                break;
            }
        }

    };

    g.input = input;

    return g;
}(game || {}));