/**
 * @file Contains functions for WEBRTC (peer.js)
 */

var game = (function (g) {
    "use strict";

    var c = {}

    /**
     * Audio element
     * @type {DOM element}
     */
    c.selfView = undefined

    /**
     * Audio element
     * @type {DOM element}
     */
    c.remoteView = undefined

    /**
     * Your peer ID.
     * @type {string}
     */
    c.peerId = undefined

    /**
     * Peer ID of other player.
     * @type {string}
     */
    c.remotePeerId = undefined

    /**
     * Call object of Peer.
     * @type {peer.call object}
     */
    c.call = undefined

    /**
     * Peer object.
     * @type {Peer instance}
     */
    c.peer = undefined

    /**
     * Initialize call stuff, it's called when client joins private room for 2 players.
     * @constructor
     */
    c.Init = function () {
        c.selfView = document.createElement("audio");
        c.selfView.autoplay = "autoplay";
        c.selfView.muted = false;

        c.remoteView = document.createElement("audio");
        c.remoteView.autoplay = "autoplay";
        c.remoteView.muted = false;

        navigator.getUserMedia = ( navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia);

        if (!navigator.getUserMedia) {
            console.debug("getUserMedia not supported");
            g.NewMessage($("<div>").html("### Your browser doesn't support WebRTC!" +
            "<br>You can't make ingame calls.").css('color', 'red'))
            return
        }

        /**
         * Init peer.js
         * @type {Window.Peer}
         */
        c.peer = new Peer({
            key: 'bp2o8cc03t7yzaor', //use your key here
            debug: 3, //remove in production
            config: {
                'iceServers': [
                    {url: 'stun:stun.l.google.com:19302'} // Pass in optional STUN and TURN server for maximum network compatibility
                ]
            }
        });

        /**
         * On peer.js ready.
         */
        c.peer.on('open', function (id) {
            console.debug('My peer ID is: ' + id);
            c.peerId = id
            c.SendPeerID()
        });

        /*c.peer.on('connection', function(conn) {
         console.debug('Peer connected')
         });*/

        $("#call_answer").click(function () {
            navigator.getUserMedia(
                {
                    video: false,
                    audio: true
                },
                function (stream) {
                    game.webrtc.call.answer(stream); // Answer the call with an A/V stream.
                    game.webrtc.call.on('stream', function (remoteStream) {
                        game.webrtc.remoteView = attachMediaStream(game.webrtc.remoteView, remoteStream) //temasys
                        //game.webrtc.remoteView.src = window.URL.createObjectURL(remoteStream);
                        c.CallStarted()
                    });
                    game.webrtc.call.on('close', function () {
                        c.CallEnded()
                    })
                    game.webrtc.call.on('close', function () {
                        c.CallEnded()
                    })
                },

                // errorCallback
                function (err) {
                    console.debug("The following error occured: " + err);
                    console.debug(err)
                    g.NewMessage($("<div>").html("### Failed to get media. Did you allow microphone use?").css('color', 'red'))
                }
            );

            $("#incoming_call").hide()
        })

        $("#call_decline").click(function () {
            game.webrtc.call.close();
            $("#webrtc_call_button").val("Call")
            $("#incoming_call").hide()
        })

        /**
         * On incoming call.
         */
        c.peer.on('call', function (callobj) {
            $("#webrtc_call_button")[0].disabled = false
            $("#webrtc_call_button").val("End call")
            console.debug("Incoming call")

            if (game.webrtc.call) {
                game.webrtc.call.close();
            }

            game.webrtc.call = callobj
            $("#incoming_call").show()
        });

        /**
         * When server tells peer id of other player.
         */
        g.socket.on('send peerId', function (data) {
            c.remotePeerId = data.id
            $("#webrtc_call_button")[0].disabled = false
            console.debug("Got other side peer ID:" + c.remotePeerId)
        })

        /**
         * On call button click.
         */
        $("#webrtc_call_button").click(function () {
            if (c.call === undefined || !c.call.open)
                game.webrtc.Call()
            else {
                c.call.close()
                $("#webrtc_call_button").val("Call")
            }
        })
    }

    c.CallEnded = function(){
        $("#bg_music").prop('volume', 0.6);
        $("#webrtc_call_button").val("Call")
    }

    c.CallStarted = function(){
        if(!$("#bg_music").prop('muted') && $("#bg_music").prop('volume') > 0.5)
            $("#bg_music").prop('volume', 0.4);
    }

    /**
     * Report peer id to server.
     */
    c.SendPeerID = function () {
        g.socket.emit('send peerId', {id: c.peerId});
    }

    /**
     * Show call button.
     */
    c.ShowManager = function () {
        $("#webrtc_call_button").show()
        g.socket.emit('get peerId');
    }

    /**
     * Make call to other player.
     */
    c.Call = function () {
        $("#webrtc_call_button").val("End call")
        if (c.remotePeerId !== undefined) {
            navigator.getUserMedia(
                {
                    video: false,
                    audio: true
                },
                function (localMediaStream) {
                    c.call = c.peer.call(c.remotePeerId, localMediaStream);
                    c.call.on('stream', function (remoteStream) {
                        game.webrtc.selfView = attachMediaStream(game.webrtc.selfView, remoteStream)
                        //game.webrtc.selfView.src = window.URL.createObjectURL(remoteStream);
                        //window.localStream = remoteStream;
                        c.CallStarted()
                    });
                    c.call.on('close', function () {
                        c.CallEnded()
                    })
                    c.call.on('close', function () {
                        c.CallEnded()
                    })
                },

                // errorCallback
                function (err) {
                    console.debug("The following error occured: " + err);
                    console.debug(err)
                    g.NewMessage($("<div>").html("### Failed to get media. Did you allow microphone use?").css('color', 'red'))
                }
            );
        } else {
            console.debug("Other side didn't report peer ID yet.")
            g.socket.emit('get peerId');
        }
    }

    g.webrtc = c

    return g;
}(game || {}));