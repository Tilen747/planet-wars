/**
 * @file Helper file for planets.
 */

var game = (function (g) {
    "use strict";

    /**
     * List of planets.
     * @type {Array}
     */
    g.planets = [];

    /**
     * Array of catched planets.  { id }
     * @type {null}
     */
    g.planetsCatch = {};

    /**
     * Clear planets
     */
    g.ClearPlanets = function(){
        g.planets =  []
        g.planetsCatch = {}
    }

    /**
     * Delete planet with given id.
     * @param planetId
     * @constructor
     */
    g.DeletePlanet = function(planetId){
        var index = g.GetPlanetIndexById(planetId)
        if(index !== undefined) {
            g.planets[index].isGhost = true
            game.particleHelper.Explode(Math.ceil(g.planets[index].r),
                g.planets[index].x,
                g.planets[index].y,
                0.001,
                0.01,
                undefined, undefined,
                {color: g.planets[index].color, fadeOut: 0.000055, shrink: 0.00001})
            g.planets.splice(index, 1)
            g.RebuildPlanetsCatch()
        }
    }

    /**
     * Rebuild/create planets with data send from server.
     * @param planets
     * @param lastUpdate - passed time from last update of this planet  //NOT IN USE
     * @constructor
     */
    g.RebuildPlanets = function (planets, lastUpdate) {

        var waitingList = [] // {index, orbitAroundPlanetId}

        var transferDelay = g.ping / 2

        for (var i = 0; i < planets.length; i++) {
            var index = -1;

            if(planets[i].isGhost)
                continue

            if(g.planetsCatch[planets[i].id] !== undefined){
                index = g.planetsCatch[planets[i].id];
                g.planets[index].Set(planets[i]);
                //if(planets[i].transferTime !== undefined)
                if(transferDelay != 0)
                    g.UpdatePlanet(index, /*planets[i].transferTime + */transferDelay );
            } else {
                index = g.planets.length;
                g.planets.push(new game.models.Planet(planets[i]).RecalculateDestination());
            }
            if(planets[i].orbitAroundPlanetId !== undefined){
                waitingList.push({ index: index, orbitAroundPlanetId: planets[i].orbitAroundPlanetId });
            }
            if(g.planets[index].ownerId != -1)
                g.planets[index].color = g.GetPlayerColor(g.planets[index].ownerId)
            g.planets[index].isGhost = false
        }

        g.RebuildPlanetsCatch();

        for (var i = 0; i < waitingList.length; i++) {
            if(g.planetsCatch[waitingList[i].orbitAroundPlanetId] !== undefined){
                g.planets[waitingList[i].index].OrbitAroundPlanet(g.planets[g.planetsCatch[waitingList[i].orbitAroundPlanetId]])
                console.debug("Added "+(waitingList[i].index)+" around planet id" + g.planetsCatch[waitingList[i].orbitAroundPlanetId]);
            } else {
                console.warn("Uknown planet id: " + waitingList[i].orbitAroundPlanetId);
            }
        }
    };

    /**
     * Update only one planet.
     * @param planetIndex
     * @param gameTime
     * @constructor
     */
    g.UpdatePlanet = function(planetIndex, gameTime){
        g.planets[planetIndex].UpdateLogic(gameTime);
        g.planets[planetIndex].Update(gameTime);
    };

    //INTERACTION
    /**
     * List of selected planets.
     * @type {Array}
     */
    g.selectedPlanets = []; //{planet, index}

    /**
     * Select planet.
     * @param index - index of planet
     * @constructor
     */
    g.SelectPlanet = function(index){
        if(!g.planets[index].isPassive) {
            if (g.selectedPlanets.length != 0) {
                if (g.selectedPlanets[0].planet.id !== g.planets[index].id) {
                    if(g.selectedPlanets[0].planet.selected == 1) {
                        g.AttackPlanet(index);
                        g.planets[index].Animate({type: 'blink', color: 'red'})
                        g.DeselectPlanets();
                    } else if(g.selectedPlanets[0].planet.ownerId == g.planets[index].ownerId && g.selectedPlanets[0].planet.selected == 2){
                            g.selectedPlanets[0].planet.transferLine = g.planets[index]
                            g.DeselectPlanets();

                    } else {
                        console.warn("Planet Shouldn't be selected (planet id: "+g.selectedPlanets[0].planet.id+").")
                    }
                } else {

                        g.planets[index].selected++
                        if (g.planets[index].selected >= 3) {
                            g.planets[index].transferLine = null
                            g.DeselectPlanets()
                        }

                }
            } else {
                if (g.planets[index].ownerId == g.playerId) {
                    g.selectedPlanets = []
                    g.selectedPlanets.push({planet: g.planets[index]});
                    g.planets[index].selected = 1;
                } else {
                    console.debug("This is not you planet.")
                }
            }
        } else {
            console.debug("You can't select passive element.")
        }
    };

    /**
     * Deselects all planets.
     * @constructor
     */
    g.DeselectPlanets = function(){
        for(var i = 0; i < g.selectedPlanets.length; i++){
            g.selectedPlanets[i].planet.selected = 0;
        }
        g.selectedPlanets = [];
    }

    /**
     * Attack planet from selected planets.
     * @param index
     * @constructor
     */
    g.AttackPlanet = function(index){
        if(index === undefined) {
            console.warn('Planet index undefined!')
            return
        }
        var source = [];

        for(var i = 0; i < g.selectedPlanets.length; i++){
            source.push(g.selectedPlanets[i].planet.id);
        }

        //console.debug(index)
        //console.debug(g.selectedPlanets);

        g.socket.emit('attack planet', {
            sources: source,
            target: g.planets[index].id
        });
    };

    /**
     * Rebuilds planets catch
     * @constructor
     */
    g.RebuildPlanetsCatch = function(){
        g.planetsCatch = {};
        for(var i = 0; i < g.planets.length; i++){
            g.planetsCatch[g.planets[i].id] = i;
        }
    };

    /**
     * Return index of planet by his id, if not successful return undefined.
     * @param id - id of planet
     * @returns {Number, undefined}
     * @constructor
     */
    g.GetPlanetIndexById = function(id){
        if(g.planetsCatch === null){
            g.RebuildPlanetsCatch();
        }
        return g.planetsCatch[id];
    };

    return g;
}(game || {}));