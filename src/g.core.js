/**
 * @file Main fail with core logic for game.
 */

var game = (function (g) {
    "use strict";

    /**
     * If FPS monitor should be updated.
     * @type {boolean}
     */
    g.statsEnabled = false

    /**
     * Stats object for FPS monitor.
     * @type {Stats}
     */
    g.stats = undefined; //FPS graph

    /**
     * Socket IO object
     * @type {Socket IO}
     */
    g.socket = undefined;

    /**
     * Id of player (this is told by server when client joins room at registered event).
     * @type {Number}
     */
    g.playerId = undefined;

    /**
     * If set to true extra code will be executed at end of Draw function which is normally not.
     * Example: when camera position is changed this will be set to true to regenerate drawings on background canvas.
     * @type {boolean}
     */
    //FIXME not same as camera change!
    g.todo = false //FLAG

    /**
     * If debug is enabled.
     * @type {boolean}
     */
    g.debug = false

    /**
     * Black hole object. Tells if black hole should be drawn and where to draw it.
     * @type {{enabled: boolean, x: number, y: number, grd: undefined, Init: Function, Draw: Function}}
     */
    g.blackHole = {
        enabled: true,
        x: 0,
        y: 0,

        grd: undefined, //ctx gradient

        UpdateGrd: function(){
            var x = (g.blackHole.x + g.camera.x) * g.camera.zoom
            var y = (g.blackHole.y + g.camera.y) * g.camera.zoom
            var r = 30 * g.camera.zoom

            this.grd = g._draw.ctx2.createRadialGradient(x, y, r / 2, x, y, r);
            this.grd.addColorStop(0, "black");
            this.grd.addColorStop(0.2, "rgba(0,0,0,0.5)");
            this.grd.addColorStop(1, "rgba(0,0,0,0)");
        },

        /**
         * Draw black hole.
         */
        Draw: function(){
            var x = (g.blackHole.x + g.camera.x) * g.camera.zoom
            var y = (g.blackHole.y + g.camera.y) * g.camera.zoom
            var r = 30 * g.camera.zoom

            if(!this.grd) {
                this.UpdateGrd()
            }

            g._draw.ctx2.beginPath();

            g._draw.ctx2.arc(x, y, r, 0, Math.PI * 2, true);
            g._draw.ctx2.fillStyle = this.grd
            g._draw.ctx2.fill();

            g._draw.ctx2.closePath();
        }
    };

    /**
     * List of particles objects.
     * @type {Array}
     */
    g.particles = []

    /**
     * List of stars objects.
     * @type {Array}
     */
    g.stars = []

    /**
     * Ping in [ms]
     * @type {number}
     */
    g.ping = 0;

    /**
     * Timout for ping function.
     * @type {timeOut}
     */
    g.pingingTimeout = undefined;

    /**
     * Start pinging.
     */
    g.StartPinging = function () {
        g.pingingTimeout = setTimeout(function () {
            g.socket.emit('ping', Date.now());
            g.StartPinging()
        }, 5000)
    }

    /**
     * Stop pinging.
     */
    g.StopPinging = function () {
        clearTimeout(g.pingingTimeout)
    }

    /**
     * Extra time to pass before some local event cheks with server if acction was reallly triggered, if server didn't responded in that time
     * @type {Number}
     */
    g.timeBeforeStatusRequest = 100;

    /**
     * Object of players. Id of players is object key.
     * @type {{me, color},...}
     */
    g.players = {};

    /**
     * If set to true, more details will be drawn in game
     * @type {boolean}
     */
    g.drawDetails = true;

    /**
     * Last time game was redrawn.
     * @type {number}
     */
    g.lastRefreshTime = 0;

    /**
     * requestAnimationFrame for game loop
     * @type {requestAnimationFrame}
     */
    g.thinkReq = undefined;

    /**
     * Returns player color by given player id found else 'RED' (+ requests rebuild).
     * @return {string}
     */
    g.GetPlayerColor = function (playerId) {
        if (g.players[playerId] !== undefined)
            return g.players[playerId].color
        else if(playerId == -1){
            return "GRAY"
        }
        else{
            console.debug("Requesting rebuild. Because player id not found.")
            g.socket.emit('rebuild', {
                all: true
            });
            return 'RED'
        }
    }

    /**
     * Initialize client
     */
    g.InitCore = function () {


        g.socket = io.connect();


        console.debug = function (msg) {
            if (g.debug)
                console.log(msg)
        }

        g.InitClient()

        g.socket.on('pong', function (data) {
            g.SetPing(Date.now() - data)
        });

        g.socket.on('del ship', function (data) {
            g.DeleteShip(data.shipId)
            if (data.rebuild !== undefined) {
                g.RebuildSystem(data.rebuild)
            }
        });

        g.socket.on('del planet', function (data) {
          if(data.planetId !== undefined) {
            console.debug("Message from server: del planet  Planet id: " + data.planetId)
            g.DeletePlanet(data.planetId)
            //g.planets[g.planetsCatch[data.planetId]].color = "RED"
          } else {
            console.warn("Server error! Planet ID is undefined.")
          }
        });

        g.socket.on('attack planet', function (data) {
            console.debug("attack planet");
            for (var s = 0; s < data.sources.length; s++) {
                var k = g.GetPlanetIndexById(data.sources[s])
                var shipIndex = g.ships.length
                g.ships.push(new g.models.Ship({
                    x: g.planets[k].x,
                    y: g.planets[k].y,
                    color: g.GetPlayerColor(data.ownerId),
                    id: data.shipId,
                    health: g.planets[k].ships,
                    planetSourceId: data.sources[s],
                    ownerId: data.ownerId
                }).SetTarget(g.planets[g.GetPlanetIndexById(data.target)]));

                g.shipsCatch[data.shipId] = shipIndex
                g.planets[k].ships = 0
            }
        });

        g.socket.on('rebuild', function (data) {
            console.debug("Rebuild triggered")
            g.RebuildSystem(data)
        });

        g._draw.Init();

        //STATS
        g.stats = new Stats();
        g.stats.setMode(1); // 0: fps, 1: ms
        g.stats.domElement.style.position = 'absolute';
        g.stats.domElement.style.right = '0px';
        g.stats.domElement.style.top = '0px';
        g.stats.domElement.style.zIndex = 9;
        document.body.appendChild(g.stats.domElement);

        g.LoadSettings()

        g.input.Init();

        g.lastRefreshTime = new Date().getTime();
        g.Think();

        g.Refresh();
        $(window).resize(function () {
            g.Refresh();
        });

        g.scene.Start()
    };

    /**
     * Rebuild eco system of game, with given data. If object exists in system it updates it if not it creates/initialises it.
     * @param data - object { planets, ships, players, forcePlayers }   forcePlayers: if set to true, it will rebuild colors of all objects
     */
    g.RebuildSystem = function (data) {
        if (data.planets !== undefined) {
            console.debug("Rebuilding planets")
            if (data.clearPlanets)
                g.ClearPlanets()
            g.RebuildPlanets(data.planets);
        }
        if (data.ships !== undefined) {
            console.debug("Rebuilding ships")
            //g.RebuildShips()
        }
        if (data.players !== undefined) {
            g.players = data.players
            console.debug("Rebuilding players")
            for (var key in g.players) {
                g.players[key].color = g.helpFun.HslToString(g.players[key].colorHsl) // 'hsl('+g.players[key].colorHsl.h +','+g.players[key].colorHsl.l+'%,'+g.players[key].colorHsl.s+'%)'
            }
            for (var i = 0; i < g.planets.length; i++) {
                if (data.forcePlayers || g.planets[i].color == 'RED') {
                    g.planets[i].color = g.GetPlayerColor(g.planets[i].ownerId)
                }
            }
        }
    }

    /**
     * Update and draw game.
     */
    g.Think = function () {

        g.thinkReq = window.requestAnimationFrame(function (e) {

            g.Think();
            if(g.statsEnabled)
                g.stats.begin();

            var now = new Date().getTime();
            var deltaTime = now - g.lastRefreshTime;
            g.lastRefreshTime = now;

            if(g.started) {
                g.UpdateLogics(deltaTime);
                g.Update(deltaTime);
                g.Draw(deltaTime);
            }

            if(g.statsEnabled)
                g.stats.end();
        });
    };

    /**
     * Stop update and draw
     */
    g.StopThink = function () {
        window.cancelAnimationFrame(g.thinkReq);
    };

    /**
     * Update game
     * @param gameTime
     */
    g.Update = function (gameTime) {

        var pLen = g.planets.length
        for (var i = 0; i < pLen; i++) {
            if (g.planets[i].autoUpdate) {
                g.planets[i].Update(gameTime);
            }
        }

        var sLen = g.ships.length
        for (var i = 0; i < sLen; i++) {
            g.ships[i].Update(gameTime);
        }

        for (var i = 0; i < g.particles.length; i++) {
            if(g.particles[i].Update(gameTime)){
                g.particles.splice(i, 1)
                i--
            }
        }
    };

    /**
     * Logic update frequency
     * @type {number}
     */
    g.logicsUpdateFrequency = 500;

    /**
     * Last update of logics
     * @type {number}
     */
    g.lastLogicsUpdate = g.logicsUpdateFrequency;

    /**
     * Update game logics
     * @param gameTime
     */
    g.UpdateLogics = function (gameTime) {
        g.lastLogicsUpdate += gameTime;

        if (g.lastLogicsUpdate > g.logicsUpdateFrequency) {

            for (var i = 0; i < g.planets.length; i++) {
                g.planets[i].UpdateLogic(g.lastLogicsUpdate);
            }

            for (var i = 0; i < g.ships.length; i++) {
                g.ships[i].UpdateLogic(g.lastLogicsUpdate);
            }

            if (g.scene.isActive) {
                g.scene.Update(g.lastLogicsUpdate)
            }

            g.lastLogicsUpdate = 0;
        }
    };

    /**
     * Refresh resolution
     */
    g.Refresh = function () {
        var w = window.innerWidth
        var h = window.innerHeight

        g.camera.width = w
        g.camera.height = h

        g._draw.ctx1.canvas.width = w
        g._draw.ctx1.canvas.height = h
        g._draw.ctx2.canvas.width = w
        g._draw.ctx2.canvas.height = h

        $("#ingame_surface_layer1").width(w);
        $("#ingame_surface_layer1").height(h);
        $("#ingame_surface_layer2").width(w);
        $("#ingame_surface_layer2").height(h);
    };

    //FIXME 2x todo ?
    g.HandleTodo = function(){
        g.todo = false;
        $("#intro").hide()
    }

    return g;
}(game || {}));

//When everything is ready init game.
$(document).ready(function () {
    window.console = window.console || {log: function() {}}; //IE fix, prevent javascript from stopping
    game.InitCore();
});