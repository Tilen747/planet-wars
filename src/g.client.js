/**
 * @file Contains stuff that are in front line for interaction with client.
 * @author Tilen Tomakić
 */

var game = (function (g) {
    "use strict";

    /**
     * Input field for chat message.
     * @type {jQuery element}
     */
    g.chatInputElement = undefined;

    /**
     * Chat messages container. (#messages)
     * @type {jQuery element}
     */
    g.chatMessagesElement = undefined;

    /**
     * If should ping be displayed in window title.
     * @type {boolean}
     */
    g.showPing = false

    g.started = false;

    g.isPrivateRoom = false
    g.isPrivateRoomHost = undefined

    /**
     * Set new ping. It's triggered by 'pong' event.
     * @param ping
     */
    g.SetPing = function(ping){
        g.ping = ping
        if(g.showPing)
            document.title = "Planet wars - ping " + ping + " ms"
    }

    /**
     * Initialize client (socket events, register html elements).
     */
    g.InitClient = function(){

        g.RefreshRoomsList()

        $("#bg_music").prop('volume', 0.6);

        //SETTINGS
        $("#settings input[name=showPing]").click(function(){ g.SaveSettings() })
        $("#settings input[name=fpsMonitor]").click(function(){ g.SaveSettings() })
        $("#settings input[name=highDetails]").click(function(){ g.SaveSettings() })

        //On register submit
        $("#register form").submit(function( event ) {
            g.Register()
            event.preventDefault();
        });

        //when registered event from server comes
        g.socket.on('registered', function (data) {
            if (data.successful) {
                //console.log('registered ' + data.reconnectHash); //FUTURE

                g.SaveSettings(true)
                g.playerId = data.playerId;

                g.scene.Stop()
                g.ClearShips()
                g.ClearPlanets()
                g.particleHelper.Clear()

                g.RebuildSystem(data.rebuild)
                g.FocusCameraToFirstPlanet()

                g.blackHole.enabled = true
                g.StartPinging()

                g.todo = true; //so on mouse down will intro hide automatically

                if(data.password){
                    $("#pin").show()
                    $("#pin_text").text(data.password)
                    g.started = false;
                    g.isPrivateRoom = true
                    g.isPrivateRoomHost = true
                    g.webrtc.Init()
                    g.webrtc.ShowManager()
                    game.Draw(0) //!
                } else {
                    if(data.privateRoomSecondPlayer){
                        g.webrtc.Init()
                        g.webrtc.ShowManager()
                        g.isPrivateRoomHost = false
                    }
                    g.started = true;
                }

                //switch into game mode
                $("#intro").show()
                $("#player_sample").css('background-color', g.GetPlayerColor(g.playerId))
                $("#register").hide(100)
                $("#register_message").hide()
                $("#chat").show(100)
                $("body").css("overflow", "hidden")
            } else {
                $("#register_message").show().text(data.error).addClass('error').removeClass('info')
            }
        });

        //show settings box
        $("#options_button").click(function(){
            $("#settings").show()
        })

        //show chat input and focus it
        $("#chat_open_button").click(function(){
            $("#chat_form").show()
            $("#chat_open_button").hide()
            g.chatInputElement.focus()
        })

        //register chatInputElement
        g.chatInputElement = $("#chat form input[name=message]")

        //when chat input loses focus hide chat input
        g.chatInputElement.focusout(function(e){
            if(e.relatedTarget == null || e.relatedTarget.id != "chat_send"){
                $("#chat_form").hide()
                $("#chat_open_button").show()
            }
        })

        //register chatMessagesElement
        g.chatMessagesElement = $("#messages")

        //on chat message submit
        $("#chat form").submit(function( event ) {
            var msg = g.chatInputElement.val()
            var cancelClear = false

            if(msg[0] != "\\" || msg[1] == "\\") { //if isn't command send to server
                g.socket.emit('chat msg', {
                    msg: g.chatInputElement.val()
                });
            } else { //is command
                switch (msg.substr(1)){
                    case "quit":
                        g.Quit()
                        break;
                    case "stop":
                        g.StopThink()
                        break;
                    case "start":
                        g.Think()
                        break;
                    case "toggle details":
                        g.drawDetails = !g.drawDetails
                        break;
                    case "rebuild":
                        g.socket.emit('rebuild', {
                            forcePlayers: true
                        });
                        break;
                    case "toggle trail":
                        g.shipsParticleTrail = !g.shipsParticleTrail
                        break
                    case "camera fix":
                        g.FocusCameraTo(0,0,1)
                        break;
                    case "info":
                        g.NewMessage($('<div>').html(
                            "Planets: " + g.planets.length + "<br>" +
                            "Ships: " + g.ships.length + "<br>" +
                            "Particles: " + g.particles.length
                        ))
                        break;
                    case "players":
                        var list = "~~Players list:~~"
                        for(var player in g.players)
                            list += "<br> - <span style='color: "+g.GetPlayerColor(player)+"'>" + g.players[player].name + "</span>"
                        g.NewMessage($('<div>').html(list))
                        break;
                    case "help":
                        g.NewMessage($('<div>').html(
                            "List of commands:<br>" +
                            "start, stop, info, quit, rebuild, toggle details, toggle trail, camera fix<br>"
                        ))
                        break;
                    default:
                        g.NewMessage($('<div>').text("Unknown command."))
                        cancelClear = true
                        break;
                }
            }

            if(!cancelClear)  //clear chat message input if not set to true by upper code (by commands)
                g.chatInputElement.val('')

            g.chatInputElement.focus()

            event.preventDefault();
        });

        g.socket.on('start game', function (data) {
            console.log("Game started")
            g.started = true
            $("#pin").hide()
        })

        //when message comes from server
        g.socket.on('chat msg', function (data) {
            var m = $('<div>')

            if(data.isSysMsg) //is message triggered by server
                m.text("* " + data.msg).css({'color':"red", 'font-weight':'bold;'})
            else //is message from players
                m.text((g.players[data.playerId].name || "[Unknown player]") + ": " + data.msg).css('color', g.GetPlayerColor(data.playerId))

            g.NewMessage(m) //show message
        })

        g.socket.on('rooms', function (data) {
            $("#rooms span").hide()
            $("#rooms select").empty()
            $("#rooms select").append($('<option value="" disabled selected>select specific room</option>'))
            for(var n in data){
                var li = $("<option>").text( ((data[n].password)?"* ":"") + data[n].name).attr("data-val",data[n].name).val(data[n].id).attr('data-val', (data[n].password)?"1":"0")
                $("#rooms select").append(li)
            }
        })

        //when server tells that player died
        g.socket.on('player dead', function (data) {
            if(data.playerId == g.playerId && g.playerId != data.victory){ //if that player is you
                $("#intro").hide()
                $("#gameOver").show()
                g.FocusCameraTo(g.blackHole.x, g.blackHole.y, 9)
                $("#chat_form").hide()
            }
            delete g.players[data.playerId]
        });

        g.socket.on('win', function (data) {
            console.log("victory :)")
            if(data.playerId == g.playerId){ //if that player is you
                g.scene.Start()
                $("#intro").hide()
                $("#win").show()
                $("#chat_form").hide()
                $("#win_points").text(data.points)
                g.scene.fireWorks = true
                g.scene.staticPlanets = []
                g.planets = []
                g.ships= []
                g.scene.bigShipLunchTime = 3000
            }
        });
    }

    /**
     * Show message in chat.
     * @param element {jQuery element || html}
     */
    g.NewMessage = function(element){
        g.chatMessagesElement.append(element)
        setTimeout(function(){
            element.remove()
        }, 15000)
    }

    /**
     * Refresh rooms list.
     */
    g.RefreshRoomsList = function(){
        $('#rooms span').show();
        g.socket.emit('get rooms');
    }

    /**
     * Register and join game.
     * @param newPrivate -  if true request for new private room will be send
     */
    g.Register = function(newPrivate){
        var name = $("#register input[name=playerName]").val()
        if(g.helpFun.IsValidName(name) || name == "") {
            var password = undefined
            var selectedRoom = undefined
            if($("#rooms select option:selected").attr("data-val") == "1") {
                password = prompt("Please enter pin", "");
                selectedRoom = $("#rooms select").val()
            }
            if(newPrivate)
                selectedRoom = undefined
            g.socket.emit('register', {
                name: name,
                spectator: false,
                room: selectedRoom,
                private: newPrivate,
                password: password
            });
            $("#register_message").show().text('Loading ...').addClass('info').removeClass('error')
            //$("#register input[name=playerName]")[0].disabled = true
        } else {
            $("#register_message").show().text('Invalid name.').addClass('error').removeClass('info')
        }
    }

    /**
     * Save settings from html elements to local storage
     * @param cancelChanges - If True settings will not be applied.
     */
    g.SaveSettings = function(cancelChanges){
        var settings = {
            showPing: $("#settings input[name=showPing]").prop('checked'),
            fpsMonitor: $("#settings input[name=fpsMonitor]").prop('checked'),
            highDetails: $("#settings input[name=highDetails]").prop('checked'),
            name: $("#register input[name=playerName]").val()
        }

        try {
            localStorage.setItem('settings', JSON.stringify(settings))
        } catch(e){}

        g.drawDetails = (settings.highDetails === undefined) ? true : settings.highDetails
        g.showPing = settings.showPing || false
        g.statsEnabled = settings.fpsMonitor || false

        if(!cancelChanges)
            g.ApplySettings(settings)
    }

    /**
     * Apply settings.
     * @param settings - extra settings that are not available globally
     */
    g.ApplySettings = function(settings){
        $("#settings input[name=showPing]").prop('checked', g.showPing);
        $("#settings input[name=fpsMonitor]").prop('checked', g.statsEnabled);
        $("#settings input[name=highDetails]").prop('checked', g.drawDetails);

        $("#register input[name=playerName]").val(settings.name || "")

        if(g.statsEnabled)
            g.stats.domElement.style.display = 'block'
        else
            g.stats.domElement.style.display = 'none'

        document.title = "Planet wars"
    }

    /**
     * Load settings from local storage and apply them.
     */
    g.LoadSettings = function(){
        var settings = {}
        try {
            settings = localStorage.getItem('settings')
            if(settings !== null)
                settings = JSON.parse(settings)
            else
                settings = {}
        } catch(e){}

        g.drawDetails = (settings.highDetails === undefined) ? true : settings.highDetails
        g.showPing = settings.showPing || false
        g.statsEnabled = settings.fpsMonitor || false

        g.ApplySettings(settings)
    }

    /**
     * Quit game (reload window).
     */
    g.Quit = function(){
        if (confirm('Are you sure you want to quit?')) {
            location.reload();
        } else {
            $("#settings").hide()
        }
    }

    return g;
}(game || {}));


//OLD BROWSER FIXES
if (!Date.now) {
    Date.now = function() { return new Date().getTime(); };
}