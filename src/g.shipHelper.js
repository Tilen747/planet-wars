/**
 * @file core
 * @author Tilen Tomakić
 */

var game = (function (g) {
    "use strict";


    g.ships = [];

    /**
     * Clear planets
     */
    g.ClearShips= function(){
        g.ships =  []
        g.shipsCatch = {}
    }

    /**
     * Show particles trail after moving ships.
     * /!\ System intense task.
     * @type {boolean}
     */
    g.shipsParticleTrail = false;

    /**
     * Rebild ships with new data send from server.  //FIXME ship del?
     * @param ships
     * @constructor
     */
    g.RebuildShips = function(ships){
        for (var i = 0; i < ships.length; i++) {
            var shipIndex = 0
            g.ships[shipIndex].isGhost = false
            //what to do with ship timeout? problem: ship del action not triggered, ship will <-IGNORE  planet is not effected we can ignore this
        }
    }

    /**
     * Creates ship and cunfigures it to attac planet
     * @param x - X start position of ship
     * @param y - Y start position of ship
     * @param targetPlanet - {index, id} planet id or index to attack, whatewer is givven
     * @constructor
     */
    /*g.CreateShip = function(x, y, targetPlanet){
        if(targetPlanet.index === undefined){
            targetPlanet.index = g.GetPlanetIndexById(targetPlanet.id)
        }
        g.ships.push(new g.models.Ship({ x:  x, y:  y}).SetTarget(g.planets[targetPlanet.index]));
    };*/

    /**
     * Array of catched ships.  { id }
     * @type {null}
     */
    g.shipsCatch = {}

    /**
     * Rebuild ships catch
     * @constructor
     */
    g.RebuildShipCatch = function(){
        console.debug("Rebuilding catch of ships current status:")
        console.debug(g.ships)
        g.shipsCatch = {};
        for(var i = 0; i < g.ships.length; i++){
            g.shipsCatch[g.ships[i].id] = i;
        }
    };

    /**
     * return sip index if found
     * @param id
     * @returns {*}
     * @constructor
     */
    g.GetShipIndexById = function(id){

        if(g.shipsCatch === null){
            g.RebuildShipCatch();
        }
        return g.shipsCatch[id];
    };

    /**
     * Ship to be deleted
     * @param shipId
     * @constructor
     */
    g.DeleteShip = function(shipId){
        console.debug("Deleting ship "+shipId)
        var shipIndex = g.GetShipIndexById(shipId);
        if(shipIndex !== undefined) {
            if(g.ships[shipIndex].statusUpdateTimeOut !== undefined)
                clearTimeout(g.ships[shipIndex].statusUpdateTimeOut)
            else
                g.ships[shipIndex].DestinationReached(true)

            g.ships.splice(shipIndex, 1)
            g.RebuildShipCatch()
        }
        else
            console.warn("No ship with id " + shipId)
    }

    return g;
}(game || {}));