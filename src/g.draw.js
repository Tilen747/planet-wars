/**
 * @file engine
 * @author Tilen Tomakić
 */

var game = (function (g) {
    "use strict";

    var _draw = {};

    _draw.ctx1 = null;
    _draw.canvasElement1;
    _draw.ctx2 = null;
    _draw.canvasElement2;

    _draw.Init = function () {
        if (this.ctx1 === null) {
            this.canvasElement1 = document.getElementById("ingame_surface_layer1");
            this.ctx1 = this.canvasElement1.getContext("2d");
        }
        if (this.ctx2 === null) {
            this.canvasElement2 = document.getElementById("ingame_surface_layer2");
            this.ctx2 = this.canvasElement2.getContext("2d");
        }
    };

    _draw.ClearCatch = function () {
        _draw.planetsCatch = null;
    };

    _draw.planetsCatch = null;

    g._draw = _draw;
    g.Draw = function () {

        g._draw.ctx2.clearRect(0, 0, g._draw.ctx2.canvas.width, g._draw.ctx2.canvas.height);

        if (g._draw.planetsCatch !== null) {
            for (var i = 0; i < g._draw.planetsCatch.length; i++) {
                g._draw.Planet(g._draw.planetsCatch[i]);
            }
        } else {
            for (var i = 0; i < g.planets.length; i++) {
                // g._draw.planetsCatch.push(i);
                //g._draw.Planet(i);
                g.planets[i].Draw();
                //g.planets[i].DrawPath();
            }
        }

        for (var i = 0; i < g.ships.length; i++) {
            g.ships[i].Draw()
        }

        for (var i = 0; i < g.particles.length; i++) {
            g.particles[i].Draw()
        }
        g._draw.ctx2.globalAlpha = 1 //reset back !!!

        if (g.blackHole.enabled) {
            g.blackHole.Draw()
        }

        if(g.camera.changed){

            g._draw.ctx1.clearRect(0, 0, g._draw.ctx1.canvas.width, g._draw.ctx1.canvas.height);

            g.blackHole.UpdateGrd()

            //g.starHelper.UpdateAndDraw()

            g.camera.changed = false
        }
    };

    return g;
}(game || {}));