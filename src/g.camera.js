/**
 * @file Contains functions and variables for camera.
 */

var game = (function (g) {
    "use strict";

    var c = {};

    /**
     * Camera x position.
     * @type {number}
     */
    c.x = 0;

    /**
     * Camera y position.
     * @type {number}
     */
    c.y = 0;

    /**
     * Width of camera.
     * @type {number}
     */
    c.width = 0;

    /**
     * Height of camera.
     * @type {number}
     */
    c.height = 0;

    /**
     * Zoom for camera.
     * @type {number}
     */
    c.zoom = 1;

    /**
     * Set to true if camera settings were changed (Draw function will at end set this back to false).
     * @type {boolean}
     */
    c.changed = true

    /* NOT IN USE
    c.onChangeEvents = [];

    c.onChange = function (event) {
        this.onchangeEvents.push(event);
    };

    c.triggerChangeEvents = function () {
        for (var i = 0; i < this.onChangeEvents.length; i++) {
            this.onChangeEvents[i]();
        }
    };
    */

    g.camera = c;

    //NOT IN CAMERA OBJECT:

    /**
     * Focus camera to first planet that belongs to this client.
     */
    g.FocusCameraToFirstPlanet = function(){
        for(var i = 0; i < g.planets.length; i++){
            if(g.planets[i].ownerId == g.playerId){
                g.FocusCameraTo(g.planets[i].x, g.planets[i].y, 2)
                return
            }
        }
        console.debug("No planet found for your id.")
    }

    /**
     * Focus camera to given point with given zoom.
     * @param x
     * @param y
     * @param zoom
     * @constructor
     */
    g.FocusCameraTo = function(x, y, zoom){
        g.camera.zoom = zoom
        g.camera.x = -x + (g.camera.width / 2 )/zoom
        g.camera.y = -y + (g.camera.height/ 2 )/zoom

        g.camera.changed = true //recalculate black hole
    }


    return g;
}(game || {}));