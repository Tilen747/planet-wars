/**
 * @file Creates scene on intro page.
 */

var game = (function (g) {
    "use strict";

    var scene = {}

    /**
     * Is scene is active.
     * @type {boolean}
     */
    scene.isActive = false

    /**
     * Stop scene.
     */
    scene.Stop = function () {
        scene.isActive = false

    }

    /**
     * If fireworks animation is enabled. (it's enabled when player wins)
     * @type {boolean}
     */
    scene.fireWorks = false

    /**
     * List of planets that don't orbit around another planet. Used by "this.Update" function to lunch ships.
     * @type {Array} - list of indexes
     */
    scene.staticPlanets = []

    /**
     * Seed for color generator.
     * @type {number}
     */
    scene.colorGen_start = 0

    /**
     * Generate new unique color based on "colorGen_start" seed.
     * @returns {hsl object}
     */
    scene.GetNextPlanetColor = function () {
        this.colorGen_start += 0.618033988749895
        this.colorGen_start %= 1
        return g.helpFun.HslToString(g.helpFun.HsvToHsl(this.colorGen_start, 0.7, 0.4))
    }

    /**
     * Start scene.
     */
    scene.Start = function () {
        g.started = true
        g.camera.x = 0
        g.camera.y = 0
        g.camera.zoom = 1
        this.bigShipLunchTime = 10000
        g.blackHole.enabled = false
        this.isActive = true
        this.colorGen_start = 0
        g.ClearShips()
        g.ClearPlanets()
        //var c = ['green', 'red', 'blue', 'orange', 'gold', 'brown']
        var k = window.innerWidth / 50
        for (var i = 0; i < k; i++) {
            g.planets.push(new g.models.Planet({
                x: Math.random() * g.camera.width,
                y: Math.random() * g.camera.height,
                r: Math.random() * 30 + 14,
                isPassive: true,
                id: i,
                speed: Math.floor((Math.random() * 10) + 1) / Math.floor((Math.random() * 15000) + 9000),
                color: this.GetNextPlanetColor()//c[ Math.floor(Math.random() * c.length) ]
            }))

            if (i != 0 && i % 3 == 0) {
                if (Math.random() > 0.5) {
                    g.planets[i].speed = -g.planets[i].speed
                }
                g.planets[i].orbit.r = Math.floor(Math.random() * 160) + 50;
                g.planets[i].OrbitAroundPlanet(g.planets[i - 1]);
            }
        }
        this.staticPlanets = []
        for (var i = 0; i < k; i++) {
            if (g.planets[i].autoUpdate)
                this.staticPlanets.push(i)
        }
    }

    /**
     * Last time a lot of ships was lunched.
     * @type {number}
     */
    scene.lastBigShipLunchTime = 7500

    /**
     * Interval of multiple ships lunch (is set in Start function).
     * @type {number}
     */
    scene.bigShipLunchTime = 0

    /**
     * Update scene.
     */
    scene.Update = function (deltaTime, doNotClearShips) {
        if (doNotClearShips !== true) {
            var shipsLength = g.ships.length;
            for (var s = 0; s < shipsLength; s++) {
                if (g.ships[s].isGhost) {
                    g.ships.splice(s, 1)
                    s--
                    shipsLength = g.ships.length;
                }
            }
        }

        if (this.fireWorks) {
            var x = Math.random() * window.innerWidth
            var y = Math.random() * window.innerHeight
            game.particleHelper.Explode(30, x, y, 0.00001, 0.14, undefined, undefined,
                { fadeOut: 0.0005, color: this.GetNextPlanetColor() }, true)
        }

        this.lastBigShipLunchTime += deltaTime
        if (this.lastBigShipLunchTime > this.bigShipLunchTime) {
            this.lastBigShipLunchTime = 0
            for (var i = 0; i < 8; i++)
                this.Update(0, true)
        }

        var l = this.staticPlanets.length
        if (l > 2) {
            var s = Math.floor((Math.random() * l) + 0);
            var t = Math.floor((Math.random() * l) + 0);
            if (t != s) {
                s = this.staticPlanets[s]
                t = this.staticPlanets[t]

                var shipId = 0
                var shipIndex = g.ships.length
                g.ships.push(new g.models.Ship({
                    x: g.planets[s].x,
                    y: g.planets[s].y,
                    color: g.planets[s].color,
                    id: shipId,
                    planetSourceId: g.planets[s].id
                }).SetTarget(g.planets[t]));
                g.shipsCatch[shipId] = shipIndex
            }
        }
    }

    g.scene = scene

    return g;
}(game || {}));